// TV Guide 

// Fetch call to: https://api.tvmaze.com/singlesearch/shows?q=${query}&embed=seasons

async function findShow(query) {
    const getShows = await fetch(`https://api.tvmaze.com/singlesearch/shows?q=${query}&embed=seasons`)
    const showsObj = await getShows.json()
    
    return showsObj;
}

findShow("rick and morty")
    .then(show => 
        document.body.innerHTML = `
            <div class="my-show-card">
                <div class="my-show-title">${show.name}</div>
                <div class="my-show-summary">${show.summary}</div>
                ${show._embedded.seasons.map(season => {
                    return `<div class="my-show-seasons">Season ${season.number}</div>`
                    })
                }
            </div>
        `
        )